package com.example.rent.sda_029_ach_to_do;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.rent.sda_029_ach_to_do.data.DatabaseHelper;
import com.example.rent.sda_029_ach_to_do.data.TaskAdapter;
import com.example.rent.sda_029_ach_to_do.data.TaskEntity;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TaskListActivity extends AppCompatActivity implements TaskAdapter.TaskCompleteListener {
    DatabaseHelper databaseHelper;
    TaskAdapter taskAdapter;
    LoadTasksFromDataBaseAsyncTask loadTasksFromDataBaseAsyncTask;
    UpdateTaskCompletedAsyncTask updateTaskCompletedAsyncTask;

    @Override
    protected void onResume() {
        super.onResume();
        loadTasksFromDataBaseAsyncTask = new LoadTasksFromDataBaseAsyncTask();
        loadTasksFromDataBaseAsyncTask.execute();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        ButterKnife.bind(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        taskAdapter = new TaskAdapter(this);
        databaseHelper = new DatabaseHelper(getApplicationContext());
        // to zastepujesz watkiem async task
//        taskAdapter.setTaskEntityList(databaseHelper.getNotCompletedTasks());


        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(taskAdapter);
    }

    @OnClick(R.id.add_task_button)
    protected void addTaskButtonClick(View view) {
        Intent intent = new Intent(TaskListActivity.this, TaskDetailsActivity.class);
        startActivity(intent);

    }



    @Override
    public void onTaskCompleted(TaskEntity taskEntity) {
        taskEntity.setDone(true);
        updateTaskCompletedAsyncTask = new UpdateTaskCompletedAsyncTask(taskEntity);
        updateTaskCompletedAsyncTask.execute();
    }

    private class LoadTasksFromDataBaseAsyncTask extends AsyncTask<Void, Void, List<TaskEntity>> {

        @Override
        protected List<TaskEntity> doInBackground(Void... params) {
            return databaseHelper.getNotCompletedTasks();
        }


        @Override
        protected void onPostExecute(List<TaskEntity> task) {
            super.onPostExecute(task);
            taskAdapter.setTaskEntityList(task);


        }
    }

    private class UpdateTaskCompletedAsyncTask extends AsyncTask<Void, Void, List<TaskEntity>> {

        TaskEntity taskEntity;

        public UpdateTaskCompletedAsyncTask(TaskEntity taskEntity) {
            this.taskEntity =  taskEntity;
        }

        @Override
        protected List<TaskEntity> doInBackground(Void... params) {
            databaseHelper.completeTask(taskEntity);
            return databaseHelper.getNotCompletedTasks();
        }

        @Override
        protected void onPostExecute(List<TaskEntity> taskEntities) {
            super.onPostExecute(taskEntities);
            taskAdapter.setTaskEntityList(taskEntities);
        }
    }

}
