package com.example.rent.sda_029_ach_to_do.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-23.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "Todo.db";
    public static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
            + ToDoContract.TaskEntry.TABLE_NAME + "("
            + ToDoContract.TaskEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + ToDoContract.TaskEntry.COLUMN_NAME_TITLE + " TEXT,"
            + ToDoContract.TaskEntry.COLUMN_NAME_DESCRIPTION + " TEXT,"
            + ToDoContract.TaskEntry.COLUMN_NAME_DONE + " INTEGER);";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // przy tworzeniu database helpera tworzy sie tabela
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    // przy zmianie wersji bazy danych tabela sie usuwa
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ToDoContract.TaskEntry.TABLE_NAME);
        onCreate(db);
    }

    // 1. save task in database
    public long insertData(TaskEntity taskEntity) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ToDoContract.TaskEntry.COLUMN_NAME_TITLE, taskEntity.getTitle());
        values.put(ToDoContract.TaskEntry.COLUMN_NAME_DESCRIPTION, taskEntity.getDescription());
        values.put(ToDoContract.TaskEntry.COLUMN_NAME_DONE, taskEntity.isDone());

        // metoda zwraca id tego wiersza gdzie robimy insert - dokumentacja insert
        // 2. return task id instead of -1
        long id = db.insert(ToDoContract.TaskEntry.TABLE_NAME, null, values);
        return id;

    }

    // 6. set particular task as completed using its id
    public void completeTask(TaskEntity task) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ToDoContract.TaskEntry.COLUMN_NAME_DONE, true);
        String selection = ToDoContract.TaskEntry._ID + " = ?";
        String[] selectionArgs = {task.getStringId()};
        db.update(ToDoContract.TaskEntry.TABLE_NAME, values, selection, selectionArgs);

    }

    // nie skonczone
    public void updateTask(TaskEntity taskEntity) {

    }

    // 3. get tasks from database which are not completed
    public List<TaskEntity> getNotCompletedTasks() {
        List<TaskEntity> taskEntities = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        String[] columns = {
                ToDoContract.TaskEntry._ID,
                ToDoContract.TaskEntry.COLUMN_NAME_TITLE,
                ToDoContract.TaskEntry.COLUMN_NAME_DESCRIPTION,
                ToDoContract.TaskEntry.COLUMN_NAME_DONE
        };
        String selection = ToDoContract.TaskEntry.COLUMN_NAME_DONE + " = ?";
        String[] selectionArgs = {"0"};

        Cursor c = db.query(ToDoContract.TaskEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);

        if (c != null && c.getCount() > 0) {
            while (c.moveToNext()) {
                long itemId = c.getLong(c.getColumnIndexOrThrow(ToDoContract.TaskEntry._ID));
                String title = c.getString(c.getColumnIndexOrThrow(ToDoContract.TaskEntry.COLUMN_NAME_TITLE));
                String description = c.getString(c.getColumnIndexOrThrow(ToDoContract.TaskEntry.COLUMN_NAME_DESCRIPTION));
                // dziwny if
                boolean completed = c.getInt(c.getColumnIndexOrThrow(ToDoContract.TaskEntry.COLUMN_NAME_DONE)) == 1;
                TaskEntity taskEntity = new TaskEntity(itemId, title, description, completed);
                taskEntities.add(taskEntity);
            }
        }
        if (c != null) {
            c.close();
        }
        return taskEntities;

    }

    // to jest nie sprawdzone
    public TaskEntity getTask(long id) {

        SQLiteDatabase db = getReadableDatabase();
        String[] columns = {
                ToDoContract.TaskEntry._ID,
                ToDoContract.TaskEntry.COLUMN_NAME_TITLE,
                ToDoContract.TaskEntry.COLUMN_NAME_DESCRIPTION,
                ToDoContract.TaskEntry.COLUMN_NAME_DONE
        };

        String selection = ToDoContract.TaskEntry._ID + " = ?";
        String[] selectionArgs = {String.valueOf(id)};

        Cursor c = db.query(ToDoContract.TaskEntry.TABLE_NAME, columns, selection, selectionArgs, null, null, null);

        TaskEntity taskEntity = null;

        if (c != null && c.getCount() > 0) {
            while (c.moveToNext()) {
                long itemId = c.getLong(c.getColumnIndexOrThrow(ToDoContract.TaskEntry._ID));
                String title = c.getString(c.getColumnIndexOrThrow(ToDoContract.TaskEntry.COLUMN_NAME_TITLE));
                String description = c.getString(c.getColumnIndexOrThrow(ToDoContract.TaskEntry.COLUMN_NAME_DESCRIPTION));
                // dziwny if
                boolean completed = c.getInt(c.getColumnIndexOrThrow(ToDoContract.TaskEntry.COLUMN_NAME_DONE)) == 0;
                taskEntity = new TaskEntity(itemId, title, description, completed);
            }
        }
        if (c != null) {
            c.close();
        }
        return taskEntity;

    }
}