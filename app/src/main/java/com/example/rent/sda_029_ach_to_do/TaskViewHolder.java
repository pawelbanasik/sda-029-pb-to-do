package com.example.rent.sda_029_ach_to_do;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RENT on 2017-05-23.
 */

public class TaskViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.text_view_title)
    public TextView title;

    @BindView(R.id.text_view_description)
    public TextView description;

    @BindView(R.id.check_box)
    public CheckBox checkBox;

    @BindView(R.id.linear_detail)
    public LinearLayout rootView;

    public TaskViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
