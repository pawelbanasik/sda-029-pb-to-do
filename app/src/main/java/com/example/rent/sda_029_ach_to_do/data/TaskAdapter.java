package com.example.rent.sda_029_ach_to_do.data;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.example.rent.sda_029_ach_to_do.R;
import com.example.rent.sda_029_ach_to_do.TaskDetailsActivity;
import com.example.rent.sda_029_ach_to_do.TaskViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-24.
 */

public class TaskAdapter extends RecyclerView.Adapter<TaskViewHolder> {

    private List<TaskEntity> taskEntityList = new ArrayList<>();
    private TaskCompleteListener taskCompleteListener;

    public TaskAdapter(TaskCompleteListener taskCompleteListener) {
        this.taskCompleteListener = taskCompleteListener;

    }


    @Override
    public TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list, parent, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TaskViewHolder holder, int position) {
        final TaskEntity taskEntity = taskEntityList.get(position);
        holder.title.setText(taskEntity.getTitle());
        holder.description.setText(taskEntity.getDescription());

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                taskEntity.setDone(true);
                taskCompleteListener.onTaskCompleted(taskEntity);
            }
        });

        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, TaskDetailsActivity.class);
                intent.putExtra(TaskDetailsActivity.INTENT_EXTRA_TASK_ID, taskEntity.getId());
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return taskEntityList.size();
    }

    public void setTaskEntityList(List<TaskEntity> taskEntities) {
        this.taskEntityList = taskEntities;
        notifyDataSetChanged();
    }

    public interface TaskCompleteListener {
        void onTaskCompleted(TaskEntity taskEntity);

    }




}
