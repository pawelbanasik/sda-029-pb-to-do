package com.example.rent.sda_029_ach_to_do.data;

/**
 * Created by RENT on 2017-05-23.
 */

public class TaskEntity {

    public static final int DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB = -1;

    private long id;
    private String title;
    private String description;
    private boolean done;

    public TaskEntity() {
        this(-1, "", "", false);
    }

    public TaskEntity(long id, String title, String description, boolean done) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.done = done;
    }

    public String getStringId(){
        String stringId = String.valueOf(id);
        return stringId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
