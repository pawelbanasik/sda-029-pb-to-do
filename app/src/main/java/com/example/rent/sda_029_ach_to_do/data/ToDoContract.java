package com.example.rent.sda_029_ach_to_do.data;

import android.provider.BaseColumns;

/**
 * Created by RENT on 2017-05-23.
 */

public final class ToDoContract {

    private ToDoContract() {
    }

    public static class TaskEntry implements BaseColumns {

        public static final String TABLE_NAME = "tasks";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_DESCRIPTION = "description";
        public static final String COLUMN_NAME_DONE = "done";



    }

}
