package com.example.rent.sda_029_ach_to_do;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.rent.sda_029_ach_to_do.data.DatabaseHelper;
import com.example.rent.sda_029_ach_to_do.data.TaskEntity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.rent.sda_029_ach_to_do.data.TaskEntity.DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB;

public class TaskDetailsActivity extends AppCompatActivity {

    public static final String INTENT_EXTRA_TASK_ID = "kluczyk";
    private boolean editMode  = true;

    @BindView(R.id.description_edit_text)
    protected EditText descriptionEditText;

    @BindView(R.id.title_edit_text)
    protected EditText titleEditText;

    private TaskEntity currentTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        ButterKnife.bind(this);

        // nie skonczone
//        long taskId = getIntent().getLongExtra(INTENT_EXTRA_TASK_ID, TaskEntity.DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB )
//
//        if (taskId != TaskEntity.DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB) {
//
//            currentTask.
//
//        } else {
//            // myk z konstruktorem w Task Entity
//            currentTask = new TaskEntity();
//        }
    }

    private void setReadOnlyMode () {
        editMode = false;
        descriptionEditText.setEnabled(false);
        titleEditText.setEnabled(false);
    }


    @OnClick(R.id.add_detail_button)
    protected void addDetailButtonClick(View v) {
        DatabaseHelper databaseHelper = new DatabaseHelper(TaskDetailsActivity.this);
        String title = titleEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        TaskEntity taskEntity = new TaskEntity(DEFAULT_ID_FOR_TASK_NOT_SAVED_IN_DB, title, description, false);
        databaseHelper.insertData(taskEntity);
        Intent intent = new Intent(TaskDetailsActivity.this, TaskListActivity.class);
        startActivity(intent);


    }
}
